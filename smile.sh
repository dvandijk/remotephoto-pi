#!/bin/bash

source smile.config

PHOTO_COMMAND="foto --capture-image-and-download"
PHOTO_DOWNLOAD=""
PHOTO_INTERVAL=""
PHOTO_FRAMES=""

# Select interval;
echo -e "Enter interval (in seconds)[1]: \c "
read interval
if [ $interval ]; then
    echo "Interval:           $interval seconds"
    PHOTO_INTERVAL=" --interval $interval "

    # Select frames;
    echo -e "Enter the number of frames[1]: \c "
    read frames
    if [ $frames ]; then
        PHOTO_FRAMES=" --frames $frames "
        echo "Frames:             $frames frames"
        runtime=$(echo "scale=2;$frames/30" | bc)
        echo "Runtime:            $runtime seconds"
        executiontime=$(echo "scale=2;$interval*$frames" | bc)
        echo "Execution time:     $executiontime seconds"
    else
        echo "No frames selected, taking unlimited frames."
    fi

else
    echo "No interval selected, taking single frame."
fi

START=$(date +%s)
$PHOTO_COMMAND $PHOTO_INTERVAL $PHOTO_FRAMES
END=$(date +%s)
DIFF=$(echo "scale=2;($END - $START)" | bc)

ttytter -runcommand="/dm $twitter_account Time lapse job done at $(date +'%d-%m-%Y %H:%M:%S'). Frames: $frames. Interval:  $interval. Execution time: $DIFF seconds"
